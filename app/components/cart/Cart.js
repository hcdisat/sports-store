(function(){ 'use strict';

    var cartData = [];

    angular.module('cart', [])
        .factory('cart', function(){
            return {
                addProduct: addProductAction,
                removeProduct: removeProductAction,
                getProducts: getProductsAction
            };
        })
        .directive('cartSummary', function(){
            return {
                restrict: 'E',
                templateUrl: 'app/components/cart/views/cartSummary.html',
                controller: ['$scope', 'cart', summaryCartController]
            };
        });

    /**
     *@param: id: productId
     *@param: name: productName
     *@param: price: productPrice
     **/
    function addProductAction(id, name, price) {
        var addedToExistingItem = false;
        for(var i = 0; i < cartData.length; i++){
            if(cartData[i].id == id){
                cartData[i].count++;
                addedToExistingItem = true;
                break;
            }
        }
        if(!addedToExistingItem){
            cartData.push({
                count: 1,
                id: id,
                name: name,
                price: price
            });
        }
    }

    /**
     * @param: id: productId
     * */
    function removeProductAction(id){
        for(var i = 0; i < cartData.length; i++){
            if(cartData[i].id == id){
                cartData.splice(i, 1);
                break;
            }
        }
    }

    /**
     * returns cartData array
     * */
    function getProductsAction(){
        return cartData;
    }

    function summaryCartController($scope, cart) {
        var cartData = cart.getProducts();

        $scope.total = function(){
            var total = 0;

            angular.forEach(cartData, function(item){
                total += item.price * item.count;
            });

            return total;
        };

        $scope.itemsCount = function(){
            var total = 0;

            angular.forEach(cartData, function(item){
                total += item.count;
            });

            return total;
        };
    }

}());