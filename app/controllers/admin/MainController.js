(function(){ 'use strict';

    var controller = [
        '$scope',
        mainController
    ];

    angular.module('sportsStoreAdmin')
        .controller('MainController', controller);

    function mainController($scope){
        $scope.screens = ['Products', 'Orders'];
        $scope.current = $scope.screens[0];

        $scope.setScreen = function(index){
            $scope.current = $scope.screens[index];
        };

        $scope.getScreen = function(){
            return $scope.current == 'Products'
                ? 'app/views/admin/adminProducts.html'
                : 'app/views/admin/adminOrders.html';
        };
    }

}());