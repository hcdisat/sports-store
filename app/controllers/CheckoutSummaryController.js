(function(){ 'use strict';

    var controller = [
        '$scope',
        'cart',
        checkoutSummaryController
    ];

    angular.module('sportsStore')
        .controller('CheckoutSummaryController', controller);

    function checkoutSummaryController($scope, cart){

        $scope.cartData = cart.getProducts();

        $scope.total = function(){
            var total = 0;

            angular.forEach($scope.cartData, function(item){
                total += item.price * item.count;
            });

            return total;
        };

        $scope.remove = function(id){
            cart.removeProduct(id);
        };
    }
}());